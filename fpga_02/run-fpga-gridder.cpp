#include <fftw3.h>

#include "common/common.h"
#include "common/print.h"
#include "common/init.h"
#include "reference/fft.h"
#include "reference/gridder.h"

int main(int argc, char **argv)
{
    if (argc > 2) {
        std::cerr << "usage: " << argv[0] << " [gridder.aocx]" << std::endl;
        exit(1);
    }

    // Initialize OpenCL
    cl::Context context;
    std::vector<cl::Device> devices;
    init(context, devices);
    cl::Device &device = devices[0];

    // Get program
    std::string filename_bin = std::string(argc == 2 ? argv[1] : "gridder.aocx");
    cl::Program program = get_program(context, device, filename_bin);

    // Allocate data structures on host
    std::clog << ">>> Initialize data structures on host" << std::endl;
    idg::Array2D<idg::UVWCoordinate<float>> uvw(NR_BASELINES, NR_TIMESTEPS);
    idg::Array3D<idg::Visibility<std::complex<float>>> visibilities(NR_BASELINES, NR_TIMESTEPS, NR_CHANNELS);
    idg::Array1D<idg::Baseline> baselines(NR_BASELINES);
    idg::Array4D<idg::Matrix2x2<std::complex<float>>> aterms(NR_TIMESLOTS, NR_STATIONS, SUBGRID_SIZE, SUBGRID_SIZE);
    idg::Array1D<float> frequencies(NR_CHANNELS);
    idg::Array1D<float> wavenumbers(NR_CHANNELS);
    idg::Array2D<float> spheroidal(SUBGRID_SIZE, SUBGRID_SIZE);
    idg::Array4D<std::complex<float>> subgrids(NR_SUBGRIDS, NR_CORRELATIONS, SUBGRID_SIZE, SUBGRID_SIZE);
    idg::Array1D<idg::Metadata> metadata(NR_SUBGRIDS);

    // Initialize spheroidal
    for (unsigned y = 0; y < SUBGRID_SIZE; y++) {
        for (unsigned x = 0; x < SUBGRID_SIZE; x++) {
            spheroidal(y, x) = 1.0f;
        }
    }

    // Initialize aterms
    for (unsigned ts = 0; ts < NR_TIMESLOTS; ts++) {
        for (unsigned station = 0; station < NR_STATIONS; station++) {
            for (unsigned y = 0; y < SUBGRID_SIZE; y++) {
                for (unsigned x = 0; x < SUBGRID_SIZE; x++) {
                    idg::Matrix2x2<std::complex<float>> aterm;
                    aterm.xx = std::complex<float>(1, 0);
                    aterm.xy = std::complex<float>(0, 0);
                    aterm.yx = std::complex<float>(0, 0);
                    aterm.yy = std::complex<float>(1, 0);
                    aterms(ts, station, y, x) = aterm;
                }
            }
        }
    }

    // Initialize random number generator
    srand(0);

    // Initialize data structures
    initialize_uvw(GRID_SIZE, uvw);
    initialize_frequencies(frequencies);
    initialize_wavenumbers(frequencies, wavenumbers);
    initialize_visibilities(GRID_SIZE, IMAGE_SIZE, frequencies, uvw, visibilities);
    initialize_baselines(NR_STATIONS, baselines);
    //initialize_spheroidal(spheroidal);
    //initialize_aterms(spheroidal, aterms);
    initialize_metadata(GRID_SIZE, NR_TIMESLOTS, NR_TIMESTEPS_SUBGRID, baselines, metadata);

    // Run reference
    std::clog << ">>> Run reference" << std::endl;
    idg::Array4D<std::complex<float>> subgrids_ref(NR_SUBGRIDS, NR_CORRELATIONS, SUBGRID_SIZE, SUBGRID_SIZE);
    kernel_gridder(
        NR_SUBGRIDS, GRID_SIZE, SUBGRID_SIZE, IMAGE_SIZE, W_STEP, NR_CHANNELS, NR_STATIONS,
        uvw.data(), wavenumbers.data(), (std::complex<float> *) visibilities.data(),
        (float *) spheroidal.data(), (std::complex<float> *) aterms.data(), metadata.data(),
        subgrids_ref.data());
    //kernel_fft(SUBGRID_SIZE, NR_SUBGRIDS, (fftwf_complex *) subgrids_ref.data(), 1);

    // Print subgrids
    print_subgrid(subgrids_ref, 0);

    // Additional data structures for FPGA
    idg::Array3D<float> lmn_fpga(SUBGRID_SIZE, SUBGRID_SIZE, 4);
    idg::Array2D<float> uvw_offsets_fpga(NR_SUBGRIDS, 4);
    idg::Array3D<float> uvw_fpga(NR_SUBGRIDS, NR_TIMESTEPS, 4);

    // Initialize lmn
    for (unsigned y = 0; y < SUBGRID_SIZE; y++) {
        for (unsigned x = 0; x < SUBGRID_SIZE; x++) {
            float l = compute_l(x, SUBGRID_SIZE, IMAGE_SIZE);
            float m = compute_m(y, SUBGRID_SIZE, IMAGE_SIZE);
            float n = compute_n(l, m);
            lmn_fpga(y, x, 0) = l;
            lmn_fpga(y, x, 1) = m;
            lmn_fpga(y, x, 2) = n;
        }
    }

    // Initialize uvw_offsets
    for (int i = 0; i < NR_SUBGRIDS; i++) {
        idg::Metadata m = metadata(i);
        idg::Coordinate c = m.coordinate;

        float w_offset_in_lambda = W_STEP * (c.z + 0.5);
        uvw_offsets_fpga(i, 0) = (c.x + SUBGRID_SIZE/2 - GRID_SIZE/2) * (2*M_PI / IMAGE_SIZE);
        uvw_offsets_fpga(i, 1) = (c.y + SUBGRID_SIZE/2 - GRID_SIZE/2) * (2*M_PI / IMAGE_SIZE);
        uvw_offsets_fpga(i, 2) = 2*M_PI * w_offset_in_lambda;
    }

    // Initialize uvw_fpga
    for (unsigned bl = 0; bl < NR_BASELINES; bl++) {
        for (unsigned time = 0; time < NR_TIMESTEPS; time++) {
            uvw_fpga(bl, time, 0) = uvw(bl, time).u;
            uvw_fpga(bl, time, 1) = uvw(bl, time).v;
            uvw_fpga(bl, time, 2) = uvw(bl, time).w;
        }
    }

    // Setup command queues
    std::vector<cl::CommandQueue> queues(8);
    for (cl::CommandQueue &queue : queues) {
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
    }

    // Allocate data structures on device
    std::clog << ">>> Allocate data structures on device" << std::endl;
    cl::Buffer d_uvw          = cl::Buffer(context, CL_MEM_READ_ONLY, uvw_fpga.bytes());
    cl::Buffer d_wavenumbers  = cl::Buffer(context, CL_MEM_READ_ONLY, wavenumbers.bytes());
    cl::Buffer d_visibilities = cl::Buffer(context, CL_MEM_READ_ONLY, visibilities.bytes());
    cl::Buffer d_spheroidal   = cl::Buffer(context, CL_MEM_READ_ONLY, spheroidal.bytes());
    cl::Buffer d_aterms       = cl::Buffer(context, CL_MEM_READ_ONLY, aterms.bytes());
    cl::Buffer d_subgrids     = cl::Buffer(context, CL_MEM_READ_WRITE, subgrids.bytes());
    cl::Buffer d_lmn          = cl::Buffer(context, CL_MEM_READ_ONLY, lmn_fpga.bytes());
    cl::Buffer d_uvw_offsets  = cl::Buffer(context, CL_MEM_READ_ONLY, uvw_offsets_fpga.bytes());
    cl::Buffer d_baselines    = cl::Buffer(context, CL_MEM_READ_ONLY, baselines.bytes());

    // Copy data structures to device
    std::clog << ">>> Copy data structures to device" << std::endl;
    queues[0].enqueueWriteBuffer(d_uvw,          CL_FALSE, 0, uvw_fpga.bytes(), uvw_fpga.data());
    queues[1].enqueueWriteBuffer(d_lmn,          CL_FALSE, 0, lmn_fpga.bytes(), lmn_fpga.data());
    queues[2].enqueueWriteBuffer(d_lmn,          CL_FALSE, 0, lmn_fpga.bytes(), lmn_fpga.data());
    queues[2].enqueueWriteBuffer(d_uvw_offsets,  CL_FALSE, 0, uvw_offsets_fpga.bytes(), uvw_offsets_fpga.data());
    queues[3].enqueueWriteBuffer(d_wavenumbers,  CL_FALSE, 0, wavenumbers.bytes(), wavenumbers.data());
    queues[4].enqueueWriteBuffer(d_visibilities, CL_FALSE, 0, visibilities.bytes(), visibilities.data());
    queues[5].enqueueWriteBuffer(d_aterms,       CL_FALSE, 0, aterms.bytes(), aterms.data());
    queues[6].enqueueWriteBuffer(d_spheroidal,   CL_FALSE, 0, spheroidal.bytes(), spheroidal.data());

    // Setup FPGA kernels
    cl::Kernel gridderKernel(program, "gridder");
    gridderKernel.setArg(0, d_wavenumbers);
    gridderKernel.setArg(1, d_lmn);
    gridderKernel.setArg(2, d_uvw);
    gridderKernel.setArg(3, d_uvw_offsets);
    gridderKernel.setArg(4, d_visibilities);
    gridderKernel.setArg(5, d_subgrids);
    gridderKernel.setArg(6, NR_SUBGRIDS);

    // Run FPGA kernels
    std::clog << ">>> Run fpga" << std::endl;
    try {
        queues[0].enqueueTask(gridderKernel);
    } catch (cl::Error &error) {
        std::cerr << "Error launching kernel: " << error.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    // Copy subgrid to host
    queues[0].enqueueReadBuffer(d_subgrids, CL_TRUE, 0, subgrids.bytes(), subgrids.data());

    // Print subgrids
    print_subgrid(subgrids, 0);

    // Compare subgrids
    std::clog << ">>> Difference between subgrids" << std::endl;
    print_subgrid_diff(subgrids_ref, subgrids, 0);

    return EXIT_SUCCESS;
}
