#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

#define USE_SIN_COS_LOOKUP_TABLE

#include "cosisin.h"

channel float input_channel_1, input_channel_2;
channel float2 output_channel_1, output_channel_2;


/*
    Data reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void data_reader(__global const volatile float *data, unsigned n)
{
    for (unsigned i = 0; i < n; i++) {
        write_channel_intel(input_channel_1, data[i]);
        write_channel_intel(input_channel_2, data[i]);
    }
}


/*
    Sine/cosine IP
*/
__kernel
__attribute__((autorun))
__attribute__((max_global_work_dim(0)))
void cosisin_ip()
{
    while (true) {
        float input   = read_channel_intel(input_channel_1);
        float2 output = (float2) (cos(input), sin(input));
        write_channel_intel(output_channel_1, output);
    }
}


/*
    Sine/cosine LU
*/
__kernel
__attribute__((autorun))
__attribute__((max_global_work_dim(0)))
void cosisin_lu()
{
    while (true) {
        float input   = read_channel_intel(input_channel_2);
        float2 output = cosisin(input);
        write_channel_intel(output_channel_2, output);
    }
}


/*
    Data writer
*/
__attribute__((max_global_work_dim(0)))
__kernel void data_writer(__global float2 *output_1, __global float2 *output_2, unsigned n)
{
    for (unsigned i = 0; i < n; i++) {
        output_1[i] = read_channel_intel(output_channel_1);
        output_2[i] = read_channel_intel(output_channel_2);
    }
}
