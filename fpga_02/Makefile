# Compiler flags
CXX = g++
CXXFLAGS = -g -std=c++11 -O2 -I..

# Altera OpenCL compiler flags
INCLUDES += $(shell aocl compile-config)
LDFLAGS += $(shell aocl link-config)
AOC = aoc

ifneq ("$(USE_SIN_COS_LOOKUP_TABLE)", "")
AOCOFLAGS += -DUSE_SIN_COS_LOOKUP_TABLE
endif

ifneq ("$(NR_GRIDDERS)", "")
AOCOFLAGS += -DNR_GRIDDERS=$(NR_GRIDDERS)
endif

AOCOFLAGS +=-I${INTELFPGAOCLSDKROOT}/include/kernel_headers
AOCXFLAGS +=${SEEDFLAGS}
AOCRFLAGS +=-fp-relaxed -fpc -report

# Emulator
AOCOFLAGS += -march=emulator -DEMULATOR
AOCRFLAGS += -emulator-channel-depth-model=strict
# export CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1

# FFTW flags
FFTW_LDFLAGS = -lfftw3f

# OpenMP flags
OPENMP_LDFLAGS = -fopenmp

CLSOURCES = gridder.cl sincos.cl
CXXSOURCES = ../common/common.cpp ../common/init.cpp ../reference/gridder.cpp run-fpga-gridder.cpp
OBJECT_FILES = $(CXXSOURCES:%.cpp=%.o)
EXECUTABLES = $(CXXSOURCES:%.cpp=%.x)

default:: run-fpga-gridder.x gridder.aocx

run-fpga-%.x: run-fpga-%.o ../common/common.o ../common/init.o ../reference/fft.o ../reference/%.o
	${CXX} ${LDFLAGS} ${FFTW_LDFLAGS} ${OPENMP_LDFLAGS} -o $@ $^

%.d: %.cpp
	${CXX} -MM -MT $*.o ${CXXFLAGS} ${INCLUDES} -o $@ $^

%.d: %.cl
	${CXX} -MM -MT $*.aoco -x c ${INCLUDES} -o $@ $^

%.o: %.cpp
	${CXX} -c ${CXXFLAGS} ${INCLUDES} -o $@ $<

%.aoco: $(SRCDIR)/%.cl
	${AOC} -c ${AOCOFLAGS} -o $@ $<

%.aoco: %.cl
	${AOC} -c ${AOCOFLAGS} -o $@ $<

%.aocr: %.aoco
	${AOC} -rtl ${AOCRFLAGS} -o $@ $<

%.aocx: %.aocr
	${AOC} ${AOCXFLAGS} -o $@ $<

clean::
	rm -rf $(EXECUTABLES) $(OBJECT_FILES)
