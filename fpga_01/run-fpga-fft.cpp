#include <fftw3.h>

#include "common/common.h"
#include "common/print.h"
#include "common/init.h"
#include "reference/fft.h"

int main(int argc, char **argv)
{
    if (argc > 2) {
        std::cerr << "usage: " << argv[0] << " [fft.aocx]" << std::endl;
        exit(1);
    }

    // Initialize OpenCL
    cl::Context context;
    std::vector<cl::Device> devices;
    init(context, devices);
    cl::Device &device = devices[0];

    // Setup parameters
    const unsigned nr_correlations = 4;
    const int subgrid_size         = 32;
    const int nr_stations          = 3;
    const int nr_timeslots         = 1;

    // Derived parameters
    auto nr_baselines = (nr_stations * (nr_stations - 1)) / 2;
    auto nr_subgrids  = nr_baselines * nr_timeslots;

    // Allocate and initialize data structures on host
    std::clog << ">>> Initialize data structures on host" << std::endl;
    idg::Array4D<std::complex<float>> subgrids(nr_subgrids, nr_correlations, subgrid_size, subgrid_size);

    // Initialize subgrids
    initialize_subgrids(subgrids);

    // Run reference
    std::clog << ">>> Run reference" << std::endl;
    idg::Array4D<std::complex<float>> subgrids_ref(nr_subgrids, nr_correlations, subgrid_size, subgrid_size);
    memcpy(subgrids_ref.data(), subgrids.data(), subgrids.bytes());
    kernel_fft(subgrid_size, nr_subgrids, (fftwf_complex *) subgrids_ref.data(), 1);

    // Setup command queues
    std::vector<cl::CommandQueue> queues(2);
    for (cl::CommandQueue &queue : queues) {
        queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);
    }

    // Allocate data structures on device
    std::clog << ">>> Allocate data structures on device" << std::endl;
    cl::Buffer d_subgrids_in  = cl::Buffer(context, CL_MEM_READ_ONLY, subgrids.bytes());
    cl::Buffer d_subgrids_out = cl::Buffer(context, CL_MEM_READ_WRITE, subgrids.bytes());

    // Copy data structures to device
    std::clog << ">>> Copy data structures to device" << std::endl;
    queues[0].enqueueWriteBuffer(d_subgrids_in, CL_FALSE, 0, subgrids.bytes(), subgrids.data());

    // Get program
    std::string filename_bin = std::string(argc == 2 ? argv[1] : "fft.aocx");
    cl::Program program = get_program(context, device, filename_bin);

    // Setup FPGA kernels
    cl::Kernel inputKernel(program, "subgrid_reader");
    inputKernel.setArg(0, d_subgrids_in);
    inputKernel.setArg(1, nr_subgrids);

    cl::Kernel outputKernel(program, "subgrid_writer");
    outputKernel.setArg(0, d_subgrids_out);
    outputKernel.setArg(1, nr_subgrids);

    // Run FPGA kernels
    std::clog << ">>> Run fpga" << std::endl;
    try {
        queues[0].enqueueTask(inputKernel);
        queues[1].enqueueTask(outputKernel);
    } catch (cl::Error &error) {
        std::cerr << "Error launching kernel: " << error.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    queues[1].finish();

    // Copy subgrid to host
    queues[1].enqueueReadBuffer(d_subgrids_out, CL_TRUE, 0, subgrids.bytes(), subgrids.data());

    // Compare subgrids
    std::clog << ">>> Difference between subgrids" << std::endl;
    print_subgrid_diff(subgrids_ref, subgrids, 0);
    print_subgrid_diff(subgrids_ref, subgrids, nr_subgrids - 1);


    return EXIT_SUCCESS;
}
