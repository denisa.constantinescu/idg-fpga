#pragma OPENCL EXTENSION cl_intel_channels : enable

#include <ihc_apint.h>

#if !defined NR_GRIDDERS
#define NR_GRIDDERS		    20
#endif

//#define USE_SIN_COS_LOOKUP_TABLE

#define SUBGRID_SIZE                32
#define NR_TIMESTEPS                128
#define NR_CHANNELS                 16
#define UNROLL_FACTOR               4U
#define NR_STATIONS                 64
#define NR_POLARIZATIONS            4

#define NR_PIXELS                   ((SUBGRID_SIZE) * (SUBGRID_SIZE))
#define NR_VISIBILITIES             ((NR_TIMESTEPS) * (NR_CHANNELS))

#define REAL                        0
#define IMAG                        1
#define COMPLEX                     2

#include "cosisin.h"
#include "fft.h"
#include "math.h"

#if NR_CHANNELS < 4 || NR_CHANNELS % 4 != 0
#error FIXME: phase_index_channel should send four phase indices
#endif


typedef float8  Visibilities[][NR_VISIBILITIES];
typedef float   UVWOffsets[][3];
typedef float   UVW[][NR_TIMESTEPS][3];
typedef float   LMN[NR_PIXELS][3];
typedef float   WaveNumbers[NR_CHANNELS];
typedef float8  Aterms[NR_STATIONS][NR_PIXELS];
typedef uint2   StationPairs[];
typedef float   Spheroidal[NR_PIXELS];
typedef float2  Subgrids[][NR_POLARIZATIONS][NR_PIXELS];


channel float8  visibilities_channel[NR_GRIDDERS] __attribute__((depth(25)));
channel float   uvw_channels[NR_GRIDDERS] __attribute__((depth(128)));
channel float   phase_offset_channel_1[NR_GRIDDERS] __attribute__((depth(NR_PIXELS))), phase_offset_channel_2[NR_GRIDDERS];
channel float   phase_index_channel_1[NR_GRIDDERS], phase_index_channel_2[NR_GRIDDERS];
channel float4  phases_channel[NR_GRIDDERS];
channel float8  phasors_channel[NR_GRIDDERS];
channel float   wave_number_channel[NR_GRIDDERS];
channel float8  pixel_channel[NR_GRIDDERS] __attribute__((depth(NR_PIXELS)));
channel float8  combined_pixel_channel, aterm_corrected_pixel_channel;
channel float2  reordered_pixel_channel, spheroidal_corrected_channel, ffted_pixel_channel;


/*
    UVW reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void uvw_reader(__global const volatile UVW uvw, unsigned nr_subgrids)
{
    #pragma loop_coalesce
    for (unsigned subgrid = 0; subgrid < nr_subgrids; subgrid += NR_GRIDDERS) {
        for (unsigned time_major = 0; time_major < NR_TIMESTEPS; time_major += 16) {
            for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                for (unsigned time_minor = 0; time_minor < 16; time_minor++) {
                    for (unsigned index = 0; index < 3; index++) {
                        float tmp = subgrid + gridder < nr_subgrids ? uvw[subgrid + gridder][time_major + time_minor][index] : 0.0f;
                        write_channel_intel(uvw_channels[gridder], tmp);
                    }
                }
            }
        }
    }
}


/*
    LMN reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void lmn_reader(__global const volatile LMN lmn, unsigned nr_subgrids)
{
    float l[NR_PIXELS], m[NR_PIXELS], n[NR_PIXELS];

    for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
        l[pixel] = lmn[pixel][0], m[pixel] = lmn[pixel][1], n[pixel] = lmn[pixel][2];
    }

    #pragma loop_coalesce 2
    for (unsigned subgrid_major = 0; subgrid_major < nr_subgrids; subgrid_major += NR_GRIDDERS) {
        for (unsigned time = 0; time < NR_TIMESTEPS; time++) {
            float uvw[NR_GRIDDERS][3] __attribute__((register));

            for (unsigned index = 0; index < 3; index++) {
                #pragma unroll
                for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                    uvw[gridder][index] = read_channel_intel(uvw_channels[gridder]);
                }
            }

            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                float phase_indices[NR_GRIDDERS];

                #pragma unroll ((NR_GRIDDERS * UNROLL_FACTOR + NR_CHANNELS - 1) / (NR_CHANNELS))
                for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                    phase_indices[gridder] = l[pixel] * uvw[gridder][0] + m[pixel] * uvw[gridder][1] + n[pixel] * uvw[gridder][2];
                }

                #pragma unroll
                for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                    write_channel_intel(phase_index_channel_1[gridder], phase_indices[gridder]);
                }
            }
        }
    }
}


/*
    Phase index repeater
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phase_index_repeater()
{
    int gridder = get_compute_id(0);

    while (true) {
        float phase_indices[NR_PIXELS] __attribute__((max_concurrency(2)));

        for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
            phase_indices[pixel] = read_channel_intel(phase_index_channel_1[gridder]);
        }

        #pragma loop_coalesce
        for (unsigned short chan = 0; chan < NR_CHANNELS; chan += UNROLL_FACTOR) {
            for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
                write_channel_intel(phase_index_channel_2[gridder], phase_indices[pixel]);
            }
        }
    }
}


/*
    UVW offsets reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void uvw_offsets_reader(__global const volatile UVWOffsets *restrict uvw_offsets, __global const volatile LMN *restrict lmn, unsigned nr_subgrids)
{
    #pragma loop_coalesce 3
    for (unsigned subgrid = 0; subgrid < nr_subgrids;) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++, subgrid++) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                float phase_offset = 0;

                #pragma unroll 1
                for (uint2_t index = 0; index < 3; index++) {
                    float uvw_offset = subgrid < nr_subgrids ? (*uvw_offsets)[subgrid][index] : 0.0f;
                    float lmn_value = (*lmn)[pixel][index];
                    phase_offset += uvw_offset * lmn_value;
                }

                write_channel_intel(phase_offset_channel_1[gridder], phase_offset);
            }
        }
    }
}


/*
    Phase offset repeater
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phase_offset_repeater()
{
    int gridder = get_compute_id(0);

    while (true) {
        float _phase_offsets[NR_PIXELS] __attribute__((max_concurrency(2)));;

        for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
            _phase_offsets[pixel] = read_channel_intel(phase_offset_channel_1[gridder]);
        }

        #pragma loop_coalesce 2
        for (unsigned vis_major = 0; vis_major < NR_VISIBILITIES; vis_major += UNROLL_FACTOR) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                write_channel_intel(phase_offset_channel_2[gridder], _phase_offsets[pixel]);
            }
        }
    }
}


/*
    Wavenumber reader
*/
__attribute__((max_global_work_dim(0)))
#if defined EMULATOR
__kernel void wave_numbers_reader(__global const volatile WaveNumbers wave_numbers, __global const unsigned *nr_subgrids_ptr) // work around emulator bug
#else
__kernel void wave_numbers_reader(__global const volatile WaveNumbers wave_numbers, unsigned nr_subgrids)
#endif
{
#if defined EMULATOR
    unsigned nr_subgrids = *nr_subgrids_ptr;
#endif

    WaveNumbers _wave_numbers;

    #pragma unroll 1
    for (unsigned chan = 0; chan < NR_CHANNELS; chan++) {
        _wave_numbers[chan] = wave_numbers[chan];
    }

    #pragma loop_coalesce
    for (unsigned subgrid_major = 0; subgrid_major < nr_subgrids; subgrid_major += NR_GRIDDERS) {
        for (unsigned time = 0; time < NR_TIMESTEPS; time++) {
            for (unsigned chan = 0; chan < NR_CHANNELS; chan++) {
                #pragma unroll
                for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                    write_channel_intel(wave_number_channel[gridder], _wave_numbers[chan]);
                }
            }
        }
    }
}


/*
    Visibilities reader
*/
__attribute__((max_global_work_dim(0)))
__kernel void visibilities_reader(__global const volatile Visibilities visibilities, unsigned nr_subgrids)
{
    #pragma loop_coalesce 4
    for (unsigned subgrid_major = 0; subgrid_major < nr_subgrids; subgrid_major += NR_GRIDDERS) {
        for (unsigned vis_major = 0; vis_major < NR_VISIBILITIES; vis_major += 16) {
            for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
                for (unsigned vis_minor = 0; vis_minor < 16; vis_minor++) {
                    float8 visibility = subgrid_major + gridder < nr_subgrids ? visibilities[subgrid_major + gridder][vis_major + vis_minor] : (float8) 0;
                    write_channel_intel(visibilities_channel[gridder], visibility);
                }
            }
        }
    }
}

//// Phases

__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phases()
{
  int gridder = get_compute_id(0);

  while (true) {
    float4 wave_numbers;

    for (unsigned short vis_minor = 0; vis_minor < UNROLL_FACTOR; vis_minor++)
      wave_numbers[vis_minor] = read_channel_intel(wave_number_channel[gridder]);

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
      float  phase_offset = read_channel_intel(phase_offset_channel_2[gridder]);
      float  phase_index  = read_channel_intel(phase_index_channel_2[gridder]);
      float4 phases       = -phase_index * wave_numbers + phase_offset;
      write_channel_intel(phases_channel[gridder], phases);
    }
  }
}


/*
    Phasors
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void phasors()
{
    int gridder = get_compute_id(0);

    while (true) {
        float4 phases = read_channel_intel(phases_channel[gridder]);
        float2 phasors[UNROLL_FACTOR];

        #pragma unroll
        for (unsigned i = 0; i < UNROLL_FACTOR; i++) {
            phasors[i] = cosisin(phases[i]);
        }

        write_channel_intel(phasors_channel[gridder], (float8) (phasors[0], phasors[1], phasors[2], phasors[3]));
    }
}


/*
   Gridder
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__attribute__((num_compute_units(NR_GRIDDERS)))
__kernel void gridder()
{
    int    gridder = get_compute_id(0);
    float8 subgrid[NR_PIXELS];

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
        subgrid[pixel] = 0;
    }

    #pragma ivdep
    for (unsigned short vis_major = 0; vis_major < NR_VISIBILITIES; vis_major += UNROLL_FACTOR) {
        float8 visibilities[UNROLL_FACTOR] __attribute__((register));

        for (unsigned short vis_minor = 0; vis_minor < UNROLL_FACTOR; vis_minor++) {
            visibilities[vis_minor] = read_channel_intel(visibilities_channel[gridder]);
        }

        for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
            float8 pixel_value = subgrid[pixel];
            float8 phasors     = read_channel_intel(phasors_channel[gridder]);

            #pragma unroll
            for (unsigned short vis_minor = 0; vis_minor < UNROLL_FACTOR; vis_minor++) {
                pixel_value.even += phasors[vis_minor * COMPLEX + REAL] * visibilities[vis_minor].even + -phasors[vis_minor * COMPLEX + IMAG] * visibilities[vis_minor].odd;
                pixel_value.odd  += phasors[vis_minor * COMPLEX + REAL] * visibilities[vis_minor].odd  +  phasors[vis_minor * COMPLEX + IMAG] * visibilities[vis_minor].even;
            }

            subgrid[pixel] = pixel_value;
        }
    }

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
        write_channel_intel(pixel_channel[gridder], subgrid[pixel]);
    }
}


/*
    Multiplexer
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__kernel void pixel_channel_multiplexer()
{
    #pragma loop_coalesce
    for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++) {
        for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
            write_channel_intel(combined_pixel_channel, read_channel_intel(pixel_channel[gridder]));
        }
    }
}


/*
    Aterm
*/
__attribute__((max_global_work_dim(0)))
__kernel void aterm(
    __global const volatile Aterms *restrict aterms,
    __global const volatile StationPairs *restrict stationPairs,
    unsigned nr_subgrids)
{
    #pragma loop_coalesce 2
    for (unsigned subgrid = 0; subgrid < nr_subgrids;) {
        for (unsigned gridder = 0; gridder < NR_GRIDDERS; gridder++, subgrid++) {
            uint2 stationPair;

            if (subgrid < nr_subgrids) {
                stationPair = (*stationPairs)[subgrid];
            }

            for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
                float8 pixel_value = read_channel_intel(combined_pixel_channel);

                if (subgrid < nr_subgrids) {
                    float8 aterm[2];

                    #pragma unroll 1
                    for (uint2_t s = 0; s < 2; s++) {
                        aterm[s] = (*aterms)[stationPair[s]][pixel];
                    }

                    pixel_value = matmul(aterm[1], matmul(pixel_value, hermitian(aterm[0])));
                    write_channel_intel(aterm_corrected_pixel_channel, pixel_value);
                }
            }
        }
    }
}


/*
    Pixel polarization splitter
*/
__attribute__((max_global_work_dim(0)))
__attribute__((autorun))
__kernel void pixel_polarization_splitter()
{
#if NR_PIXELS == 1024 && NR_POLARIZATIONS == 4
    float2 pixels_1[NR_PIXELS], pixels_2[NR_PIXELS], pixels_3[NR_PIXELS];

    for (uint12_t i = 0;; i++) {
        uint2_t  pol   = i / NR_PIXELS;
        uint10_t pixel = i % NR_PIXELS;
        float8   tmp;

        switch (pol) {
            case 0:
                tmp = read_channel_intel(aterm_corrected_pixel_channel);
                pixels_1[pixel] = tmp.s23;
                pixels_2[pixel] = tmp.s45;
                pixels_3[pixel] = tmp.s67;
                break;

            case 1:
                tmp.s01 = pixels_1[pixel];
                break;

            case 2:
                tmp.s01 = pixels_2[pixel];
                break;

            case 3:
                tmp.s01 = pixels_3[pixel];
                break;
        }

        write_channel_intel(reordered_pixel_channel, tmp.s01);
    }
#else
    union {
        float8 f8;
        float2 f2[4];
    } pixels[NR_PIXELS];

    for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
        pixels[pixel].f8 = read_channel_intel(aterm_corrected_pixel_channel);
    }

    for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
        for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
            write_channel_intel(reordered_pixel_channel, pixels[pixel].f2[pol]);
        }
    }
#endif
}


/*
    Spheroidal
*/
__attribute__((max_global_work_dim(0)))
__kernel void spheroidal(__global const volatile Spheroidal spheroidal, unsigned nr_subgrids)
{
    Spheroidal _spheroidal;

    for (unsigned short pixel = 0; pixel < NR_PIXELS; pixel++) {
        _spheroidal[pixel] = spheroidal[pixel];
    }

    #pragma loop_coalesce
    for (unsigned count = 0; count < nr_subgrids * NR_POLARIZATIONS; count++) {
        for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
            write_channel_intel(spheroidal_corrected_channel, _spheroidal[pixel] * read_channel_intel(reordered_pixel_channel));
        }
    }
}


/*
    FFT kernel
*/
__kernel
__attribute__((autorun))
__attribute__((max_global_work_dim(0)))
void fft_kernel()
{
    float2 data[32][32] __attribute__((doublepump, max_concurrency(2)));

    #pragma loop_coalesce 2
    for (unsigned short y = 0; y < SUBGRID_SIZE; y++) {
	for (unsigned short x = 0; x < SUBGRID_SIZE; x++) {
	    data[y][x] = read_channel_intel(spheroidal_corrected_channel);
	}
    }

    fft_32x32(data, FFT_BACKWARD);

    #pragma loop_coalesce 2
    for (unsigned short y = 0; y < SUBGRID_SIZE; y++) {
	for (unsigned short x = 0; x < SUBGRID_SIZE; x++) {
	    write_channel_intel(ffted_pixel_channel, data[bit_reverse_32(y)][bit_reverse_32(x)]);
	}
    }
}


/*
    Subgrid writer
*/
__attribute__((max_global_work_dim(0)))
__kernel void subgrid_writer(__global Subgrids subgrids, unsigned nr_subgrids)
{
    #pragma loop_coalesce 3
    for (unsigned subgrid = 0; subgrid < nr_subgrids; subgrid++) {
        for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
            for (unsigned pixel = 0; pixel < NR_PIXELS; pixel++) {
                subgrids[subgrid][pol][pixel] = read_channel_intel(ffted_pixel_channel);
            }
        }
    }
}
