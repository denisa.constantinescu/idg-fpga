# Introduction
This repository contains IDG implementations in OpenCL for Intel Arria 10 FPGAs, including code to test correctness against a reference CPU implementation.

# Environment
Make sure that the Intel FPGA SDK for OpenCL toolkit is installed and the enviroment is set-up (e.g. the `aoc` and `aocl` commands are working and `INTELOCLSDKROOT` is set.)
The test programs by default use the FPGA emulator, make sure it is enabled: `export CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1`.

# Build and run FPGA tests
```
make
./run-fpga-gridder.x
./run-fpga-degridder.x
./run-fpga-sincos.x
./run-fpga-fft.x
```
